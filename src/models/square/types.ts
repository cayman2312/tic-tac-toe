export type MovesType = "x" | "o";
export type CellType = {
  data: MovesType | null;
  isWon: boolean;
};

export type SetCellData = {
  rowIndex: number;
  columnIndex: number;
};

export type GameStatus = "run" | "finish";

export type SendData = {
  squareData: CellType[][];
  currentMove: MovesType;
};

export type ActionType = [number, number]

export type AnswerType = {
  move: MovesType,
  action: ActionType
}
