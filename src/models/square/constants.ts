export const SQUARE_COLUMNS = Number(process.env.REACT_APP_SQUARE_COLUMNS) || 5;
export const SQUARE_ROWS = Number(process.env.REACT_APP_SQUARE_ROWS) || 5;
// export const SQUARE_SIZE = Number(process.env.REACT_APP_SQUARE_ROWS) || 3;
export const SQUARE_SIZE = 7;
export const WIN_AMOUNT = Number(process.env.REACT_APP_WIN_AMOUNT) || 3;
export const CELL_WIDTH = 40
export const CELL_HEIGHT = 40

