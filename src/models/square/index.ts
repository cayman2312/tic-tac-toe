import { createStore, createEvent, restore, createEffect } from "effector";

// constants
import { SQUARE_COLUMNS, SQUARE_ROWS, SQUARE_SIZE } from "./constants";

// types
import {
  ActionType,
  AnswerType,
  CellType,
  GameStatus,
  MovesType,
  SendData,
  SetCellData,
} from "./types";

const squareData: CellType[][] = [];
for (let i = 0; i < SQUARE_ROWS; i++) {
  squareData.push(new Array(SQUARE_COLUMNS).fill({ data: null, isWon: false }));
}

export const $squareData = createStore<CellType[][]>(squareData);
export const $currentMove = createStore<MovesType>("x");
export const $gameStatus = createStore<GameStatus>("run");

export const setCellData = createEvent<SetCellData>("setCellData");
export const checkWin = createEvent<SetCellData>();
export const resetGame = createEvent();

export const changeHasSavedGame = createEvent<boolean>();
export const $hasSavedGame = restore<boolean>(changeHasSavedGame, false);

export const nextMove = createEvent("nextMove");
export const changeGameStatus =
  createEvent<{ status: GameStatus }>("changeGameStatus");

export const checkForSavedGameFx = createEffect<void, SendData | null, Error>();

export const saveFx = createEffect<SendData, SendData, Error>();
export const saveGameFx = saveFx.prepend((squareData: CellType[][]) => ({
  currentMove: $currentMove.getState(),
  squareData,
}));

export const loadGameFx = createEffect<void, SendData | null, Error>();
export const deleteGameFx = createEffect<void, void, Error>();

export const $squareSize = createStore<number>(SQUARE_SIZE);
export const raiseSquareSize = createEvent<ActionType>();

export const $usersAnswers = createStore<AnswerType[]>([]);

export const pushFullUserAnswer = createEvent<AnswerType>();

export const pushUserAnswer = pushFullUserAnswer.prepend<ActionType>(
  (action) => {

    return ({ move: $currentMove.getState(), action })
  }
);

export const $answersMap = createStore<Record<string, Record<string, MovesType>>>({})
export const setAnswersMap = createEvent<ActionType>()

export const checkWinMap = createEvent<ActionType>()

$answersMap.watch(console.log)