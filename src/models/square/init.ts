import produce from "immer";
import { forward } from "effector";
import { WIN_AMOUNT } from "./constants";
import {
  $squareData,
  $currentMove,
  $gameStatus,
  nextMove,
  setCellData,
  checkWin,
  changeGameStatus,
  resetGame,
  checkForSavedGameFx,
  changeHasSavedGame,
  loadGameFx,
  deleteGameFx,
  saveFx,
  $usersAnswers,
  pushFullUserAnswer,
  pushUserAnswer,
  raiseSquareSize,
  $squareSize,
  setAnswersMap,
  $answersMap,
    checkWinMap
} from "./index";

// api
import { Http } from "@api/fake-api/square";

// types
import {ActionType, CellType, MovesType, SetCellData} from "./types";

const setWin = (state: CellType[][], winComb: number[][]): CellType[][] => {
  changeGameStatus({ status: "finish" });

  return produce(state, (draft) => {
    winComb.forEach(([row, column]) => {
      draft[row][column].isWon = true;
    });
  });
};

const checkForWin = (
  state: CellType[][],
  { columnIndex, rowIndex }: SetCellData
) => {
  const startPoint = [rowIndex, columnIndex];
  const currentMove = $currentMove.getState();
  let winComb, i, j;

  // проверяем по горизонтали
  winComb = [[...startPoint]];
  i = columnIndex;
  while (state[rowIndex]?.[--i]?.data === currentMove) {
    winComb.push([rowIndex, i]);
  }
  i = columnIndex;
  while (state[rowIndex]?.[++i]?.data === currentMove) {
    winComb.push([rowIndex, i]);
  }

  if (winComb.length >= WIN_AMOUNT) {
    return setWin(state, winComb);
  }

  // проверяем по вертикали
  winComb = [[...startPoint]];
  i = rowIndex;
  while (state?.[--i]?.[columnIndex]?.data === currentMove) {
    winComb.push([i, columnIndex]);
  }

  i = rowIndex;
  while (state?.[++i]?.[columnIndex]?.data === currentMove) {
    winComb.push([i, columnIndex]);
  }

  if (winComb.length >= WIN_AMOUNT) {
    return setWin(state, winComb);
  }

  // проверяем по слева-снизу - направо-вверх
  winComb = [[...startPoint]];
  i = rowIndex;
  j = columnIndex;
  while (state?.[--i]?.[--j]?.data === currentMove) {
    winComb.push([i, j]);
  }
  i = rowIndex;
  j = columnIndex;
  while (state?.[++i]?.[++j]?.data === currentMove) {
    winComb.push([i, j]);
  }

  if (winComb.length >= WIN_AMOUNT) {
    return setWin(state, winComb);
  }

  // проверяем по слева-сверху - направо-вниз
  winComb = [[...startPoint]];
  i = rowIndex;
  j = columnIndex;
  while (state?.[--i]?.[++j]?.data === currentMove) {
    winComb.push([i, j]);
  }
  i = rowIndex;
  j = columnIndex;
  while (state?.[++i]?.[--j]?.data === currentMove) {
    winComb.push([i, j]);
  }

  if (winComb.length >= WIN_AMOUNT) {
    return setWin(state, winComb);
  }

  nextMove();

  return state;
};

$squareData
  .on(setCellData, (state, { columnIndex, rowIndex }) => {
    return produce(state, (draft) => {
      draft[rowIndex][columnIndex].data = $currentMove.getState();
    });
  })
  .on(checkWin, checkForWin)
  .on(loadGameFx.doneData, (state, data) => (data ? data.squareData : state))
  .reset(resetGame);

$gameStatus.on(changeGameStatus, (_, { status }) => status).reset(resetGame);

$currentMove
  .on(nextMove, (state) => (state === "x" ? "o" : "x"))
  .on(loadGameFx.doneData, (state, data) => (data ? data.currentMove : state))
  .reset(resetGame);

forward({
  from: setCellData,
  to: checkWin,
});

checkForSavedGameFx.use(() => Http.get());

checkForSavedGameFx.doneData.watch((data) => {
  data && changeHasSavedGame(true);
});

saveFx.use((data) => Http.post(data));

saveFx.done.watch(() => {
  changeHasSavedGame(true);
});

deleteGameFx.use(() => Http.delete());
deleteGameFx.done.watch(() => {
  changeHasSavedGame(false);
});

loadGameFx.use(() => Http.get());

$usersAnswers.on(pushFullUserAnswer, (state, answer) => {
  return produce(state, (draft) => {
    draft.push(answer);
  });
});

$squareSize.on(raiseSquareSize, (currentSize, [rowIndex, columnIndex]) => {
  if (
    (currentSize - 1) / 2 - Math.abs(rowIndex) < 2 ||
    (currentSize - 1) / 2 - Math.abs(columnIndex) < 2
  ) {
    return currentSize + 4;
  }

  return currentSize;
});

forward({
  from: pushUserAnswer,
  to: raiseSquareSize,
});

const checkMap = (
  state: Record<string, Record<string, MovesType>>,
  [rowIndex, columnIndex] : ActionType
) => {
  const startPoint = [rowIndex, columnIndex];
  const currentMove = $currentMove.getState();
  let winComb, i, j;

  // проверяем по горизонтали
  winComb = [[...startPoint]];
  i = columnIndex;
  while (state[rowIndex]?.[--i] === currentMove) {
    winComb.push([rowIndex, i]);
  }
  i = columnIndex;
  while (state[rowIndex]?.[++i] === currentMove) {
    winComb.push([rowIndex, i]);
  }

  if (winComb.length >= WIN_AMOUNT) {
        return console.log('по горизонтали', winComb)
    // return setWin(state, winComb);
  }

  // проверяем по вертикали
  winComb = [[...startPoint]];
  i = rowIndex;
  while (state?.[--i]?.[columnIndex] === currentMove) {
    winComb.push([i, columnIndex]);
  }

  i = rowIndex;
  while (state?.[++i]?.[columnIndex] === currentMove) {
    winComb.push([i, columnIndex]);
  }

  if (winComb.length >= WIN_AMOUNT) {
        return console.log('по вертикали', winComb)
    // return setWin(state, winComb);
  }

  // проверяем по слева-снизу - направо-вверх
  winComb = [[...startPoint]];
  i = rowIndex;
  j = columnIndex;
  while (state?.[--i]?.[--j] === currentMove) {
    winComb.push([i, j]);
  }
  i = rowIndex;
  j = columnIndex;
  while (state?.[++i]?.[++j] === currentMove) {
    winComb.push([i, j]);
  }

  if (winComb.length >= WIN_AMOUNT) {
        return console.log('слева-снизу - направо-вверх', winComb)
    // return setWin(state, winComb);
  }

  // проверяем по слева-сверху - направо-вниз
  winComb = [[...startPoint]];
  i = rowIndex;
  j = columnIndex;
  while (state?.[--i]?.[++j] === currentMove) {
    winComb.push([i, j]);
  }
  i = rowIndex;
  j = columnIndex;
  while (state?.[++i]?.[--j] === currentMove) {
    winComb.push([i, j]);
  }

  if (winComb.length >= WIN_AMOUNT) {
            return console.log('слева-сверху - направо-вниз', winComb)
    // return setWin(state, winComb);
  }

  nextMove();

  return state;
};


$answersMap.on(setAnswersMap, (answersMap, [rowIndex, columnIndex]) => {
    const currentMove = $currentMove.getState();

  return produce(answersMap, (draft) => {
    draft[rowIndex]
      ? (draft[rowIndex][columnIndex] = currentMove)
      : (draft[rowIndex] = { [columnIndex]: currentMove });
  });
}).on(checkWinMap, checkMap);


forward({
  from: pushUserAnswer,
  to: setAnswersMap,
});


forward({
  from: pushUserAnswer,
  to: checkWinMap,
});

// forward({
//   from: pushFullUserAnswer,
//   to: nextMove,
// });


