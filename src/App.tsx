import React from "react";
// import { Square } from "@components/Square";
// import { Controls } from "@components/Controls";
import { EmptySquare } from "@components/EmptySquare";
import { UserAnswers } from "@components/UserAnswers";
import { Header } from "@components/Header";

function App() {
  return (
    <div className="container max-w-3xl mx-auto box-border p-4">
      <Header />

      <div style={{height: 600}} className="relative overflow-scroll">
        <EmptySquare />

          <UserAnswers/>
      </div>

      {/*<Square />*/}

      {/*<Controls />*/}
    </div>
  );
}

export default App;
