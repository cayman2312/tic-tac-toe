import { SendData } from "@models/square/types";

export const Http = {
  get: (): Promise<SendData | null> =>
    new Promise((resolve) =>
      setTimeout(() => {
        resolve(
          localStorage.getItem("savedGame")
            ? JSON.parse(localStorage.getItem("savedGame")!)
            : null
        );
      }, 500)
    ),

  post: (data: SendData): Promise<SendData> =>
    new Promise((resolve) =>
      setTimeout(() => {
        localStorage.setItem("savedGame", JSON.stringify(data));

        resolve(data);
      }, 500)
    ),

  delete: (): Promise<void> =>
    new Promise((resolve) =>
      setTimeout(() => {
        localStorage.removeItem("savedGame");

        resolve();
      }, 500)
    ),
};
