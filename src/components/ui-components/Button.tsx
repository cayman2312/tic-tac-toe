import cx from "classnames";

type ButtonType = {
  onClick: (...args: any[]) => void;
  children: string;
  color?: "red" | "blue" | "green" | "yellow";
  shade?: 100 | 200 | 300 | 400 | 500 | 600 | 700;
  disabled?: boolean;
};

export const Button = ({
  onClick,
  children,
  shade = 400,
  color = "blue",
  disabled = false,
}: ButtonType) => {
  const cxButton = cx(
    "flex-1 border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base font-medium text-white focus:outline-none",
    {
      [`bg-${color}-${shade} hover:bg-${color}-${shade + 100}`]: !disabled,
      [`bg-${color}-200 cursor-not-allowed`]: disabled,
    }
  );

  return (
    <button onClick={onClick} className={cxButton}>
      {children}
    </button>
  );
};
