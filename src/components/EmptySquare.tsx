import { ReactElement } from "react";
import { useStore } from "effector-react";

// effector
import { $squareSize, pushUserAnswer } from "@models/square/";
import { CELL_HEIGHT, CELL_WIDTH } from "@models/square/constants";

export const EmptySquare = (): ReactElement => {
  const squareSize = useStore($squareSize);

  const data = new Array(squareSize).fill(new Array(squareSize).fill(""));

  const onCellClick = (rowIndex: number, columnIndex: number) => {
    console.log([rowIndex, columnIndex])

    pushUserAnswer([rowIndex, columnIndex]);
  };

  return (
    <table
      style={{ top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}
      className={"absolute border-collapse table-fixed "}
      width={squareSize * CELL_WIDTH}
    >
      <tbody className="divide-y">
        {data.map((row, rowIndex) => (
          <tr key={rowIndex} className="divide-x">
            {row.map((_: any, columnIndex: number) => (
              <td
                key={columnIndex}
                onClick={onCellClick.bind(
                  null,
                  (squareSize - 1) /2 - rowIndex,
                  columnIndex - (squareSize - 1) / 2
                )}
                width={CELL_WIDTH}
                height={CELL_HEIGHT}
              />
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};
