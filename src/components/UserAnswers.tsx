import React from "react";
import { useStore } from "effector-react";

// effector
import { $usersAnswers } from "@models/square";
import { CELL_HEIGHT, CELL_WIDTH } from "@models/square/constants";

export const UserAnswers = () => {
  const userAnswers = useStore($usersAnswers);

  return (
    <>
      {userAnswers.map((answer, i) => {
        return (
          <div
            key={i}
            style={{
              top: "50%",
              left: "50%",
              transform: `translate(calc(-50% + ${
                answer.action[1] * CELL_WIDTH
              }px), calc(-50% - ${answer.action[0] * CELL_HEIGHT}px))`,
              width: "40px",
              height: "40px",
            }}
            className="absolute flex justify-center items-center"
          >
            {answer.move.toUpperCase()}
          </div>
        );
      })}
    </>
  );
};
