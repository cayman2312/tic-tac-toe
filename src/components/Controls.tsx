import React, { useEffect } from "react";
import { useStore, useStoreMap } from "effector-react";

// components
import { Button } from "@uiComponents/Button";

// effector
import {
  $gameStatus,
  $hasSavedGame,
  $squareData,
  checkForSavedGameFx,
  resetGame,
  deleteGameFx,
  loadGameFx,
  saveGameFx,
} from "@models/square";

export const Controls = () => {
  const isChanged = useStoreMap($squareData, (squareData) =>
    squareData.some((row) => row.some((col) => col.data))
  );
  const gameStatus = useStore($gameStatus);
  const squareData = useStore($squareData);
  const hasSavedGame = useStore($hasSavedGame);

  useEffect(() => {
    checkForSavedGameFx();
  }, []);

  return (
    <div className="flex gap-4 mt-6 flex-wrap">
      {gameStatus === "finish" && (
        <Button onClick={resetGame} color="green">
          Начать новую игру
        </Button>
      )}

      {gameStatus === "run" && (
        <>
          <Button onClick={() => saveGameFx(squareData)} disabled={!isChanged}>
            Сохранить игру
          </Button>
          <Button onClick={loadGameFx} color="green" disabled={!hasSavedGame}>
            Загрузить игру
          </Button>
          <Button onClick={deleteGameFx} color="red" disabled={!hasSavedGame}>
            Удалить сохранение
          </Button>
        </>
      )}
    </div>
  );
};
