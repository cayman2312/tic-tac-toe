import { ReactElement } from "react";
import { useStore } from "effector-react";
import cx from "classnames";

// effector
import { $squareData, $gameStatus, setCellData } from "@models/square/";
import { SQUARE_COLUMNS } from "@models/square/constants";

// types
import { CellType } from "@models/square/types";

export const Square = (): ReactElement => {
  const data = useStore($squareData);
  const gameStatus = useStore($gameStatus);

  const onCellClick = (
    cellData: CellType["data"],
    rowIndex: number,
    columnIndex: number
  ) => {
    if (gameStatus !== "run" || cellData) return;

    setCellData({ rowIndex, columnIndex });
  };

  return (
    <div className="relative h-auto overflow-scroll flex justify-center items-center">
      <table
        className={"border-collapse table-fixed"}
        width={SQUARE_COLUMNS * 40}
      >
        <tbody className="divide-y">
          {data.map((row, rowIndex) => (
            <tr key={rowIndex} className="divide-x">
              {row.map(({ data: cellData, isWon }, columnIndex) => {
                const cxCell = cx(
                  "text-center leading-none font-light text-3xl select-none whitespace-nowrap",
                  {
                    "cursor-pointer": !cellData,
                    "bg-green-200": isWon,
                  }
                );

                return (
                  <td
                    key={columnIndex}
                    className={cxCell}
                    onClick={onCellClick.bind(
                      null,
                      cellData,
                      rowIndex,
                      columnIndex
                    )}
                    width={40}
                    height={40}
                  >
                    {cellData?.toUpperCase()}
                  </td>
                );
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
