import { useStore } from "effector-react";

// effector
import { $gameStatus, $currentMove } from "@models/square";

export const Header = () => {
  const gameStatus = useStore($gameStatus);
  const currentMove = useStore($currentMove);

  return (
    <div className="h-16 items-center shadow-md bg-white py-2 px-4 mb-6 rounded">
      <h3 className="text-gray-700">
        {gameStatus === "finish" && (
          <>
            Победил игрок <b>{currentMove.toUpperCase()}</b>
          </>
        )}
        {gameStatus === "run" && (
          <>
            Ходит игрок <b>{currentMove.toUpperCase()}</b>
          </>
        )}
      </h3>

      {gameStatus === "finish" && (
        <p className="font-light italic text-gray-700">
          Для запуска новой игры нажмите "Начать новую игру"
        </p>
      )}
    </div>
  );
};
